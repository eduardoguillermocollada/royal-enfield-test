import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

export default (component) => {
  const Constructor = Vue.extend(component)
  return (props) => new Constructor({ propsData: props }).$mount()
}