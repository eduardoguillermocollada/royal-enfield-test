import axios from 'axios'

export const state = () => ({ configs: null })

export const mutations = {
  setConfig (state, payload) {
    state.configs = payload
  }
}

export const getters = {
  getLogo ({ configs }) {
    return `http://${configs.logo.img}`
  },
  getHeaderImage ({ configs }) {
    return `http://${configs.index_header_image}`
  },
  getSocial ({ configs }) {
    return configs.social
  },
  getContact ({ configs }) {
    return configs.contact
  }
}

export const actions = {
  async nuxtServerInit ({ commit }) {
    const { data } = await axios.get('http://127.0.0.1:3001/configs')
    commit('setConfig', data)
  }
}
