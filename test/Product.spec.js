import makeExtensionOf from './utils'
import Product from '../components/Product'

const mount = makeExtensionOf(Product);

describe('Product', () => {
  const validProduct = {
    product: {
      detail: { 
        price: { 
          amount: 10000 
        },
        main_image: 'www.test.com:80/resource'
      }
    }
  }

  it('Has valid data constructor', () => {
    expect(typeof Product.data).toBe('function')
    expect(typeof Product.data()).toBe('object')
  })
  
  it('Has computed properties', () => {
    expect(typeof Product.computed).toBe('object')
  })
  
  it('Emmit valid product price', () => {
    const vm = mount(validProduct)
    expect(vm.price).toBe('$ 10000.00')
  })
  
  it('Emmit valid image url', () => {
    const vm = mount(validProduct)
    expect(vm.imageUrl).toBe('http://www.test.com:80/resource')
  })
})