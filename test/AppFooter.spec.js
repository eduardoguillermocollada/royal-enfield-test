import Vue from 'vue'
import Vuetify from 'vuetify'
import { mount } from '@vue/test-utils'

import AppFooter from '../components/AppFooter.vue'

Vue.use(Vuetify)

describe('AppFooter', () => {
  const vm = mount(AppFooter)
  
  test('is a Vue instance', () => {
    expect(vm.isVueInstance()).toBeTruthy()
  })

})